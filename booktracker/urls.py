from django.urls import path

from . import views


app_name = 'booktracker'
urlpatterns = [
    path('', views.BookList.as_view(), name='index'),
    path('create/', views.BookCreate.as_view(), name='book_create'),
    path('<int:pk>/', views.BookDetail.as_view(), name='detail'),
    path('<int:pk>/update/', views.BookUpdate.as_view(), name='book_update'),
    path('<int:book_id>/lend/', views.LoanCreate.as_view(), name='book_lend'),
    path('<int:book_id>/return/', views.book_return, name='book_return'),
]