from django import forms
from .models import Book, Loan


class AuthorForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = ['author']


class BorrowerForm(forms.ModelForm):
    class Meta:
        model = Loan
        fields = ['borrower']
