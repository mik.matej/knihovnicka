from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views.generic import DetailView, ListView, CreateView, UpdateView

from .models import Book, Loan
from .forms import AuthorForm, BorrowerForm


class BookList(ListView):
    model = Book
    template_name = 'booktracker/index.html'

    def get_queryset(self):  # how to make it prettier?
        filtered_list = Book.objects.all()
        try:
            filtered_list = filtered_list.filter(author=self.request.GET['author'])
        except KeyError:
            pass
        try:
            if self.request.GET['lent']:
                filtered_list = filtered_list.filter(is_available=False)
        except KeyError:
            pass
        try:
            id_list = Loan.objects.filter(borrower=self.request.GET['borrower']).values_list('book', flat=True)
            filtered_list = filtered_list.filter(pk__in=id_list)
        except KeyError:
            pass
        return filtered_list

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_author'] = AuthorForm()
        context['form_borrower'] = BorrowerForm()
        return context


class BookCreate(CreateView):
    model = Book
    fields = ['title', 'author', 'pub_year']
    template_name = 'booktracker/basic_form.html'
    success_url = reverse_lazy('booktracker:index')


class BookUpdate(UpdateView):
    model = Book
    fields = ['title', 'author', 'pub_year']
    template_name = 'booktracker/basic_form.html'
    success_url = reverse_lazy('booktracker:index')


class BookDetail(DetailView):
    model = Book
    template_name = 'booktracker/detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['loan_list'] = Loan.objects.filter(book=self.kwargs['pk'])
        context['form'] = BorrowerForm()
        return context


class LoanCreate(CreateView):
    model = Loan
    fields = ['borrower', 'return_deadline']
    template_name = 'booktracker/basic_form.html'
    success_url = reverse_lazy('booktracker:index')

    def form_valid(self, form):
        book = get_object_or_404(Book, id=self.kwargs['book_id'])
        book.is_available = False  # where to check if is_available was True? (too late to do it in form_valid?)
        book.save()
        form.instance.book_id = self.kwargs['book_id']
        return super().form_valid(form)


def book_return(request, book_id):
    book = get_object_or_404(Book, id=book_id)
    book.is_available = True
    book.save()
    return HttpResponseRedirect(reverse('booktracker:detail', args=(book.id,)))
